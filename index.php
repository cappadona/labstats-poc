<?php

$client = new SoapClient("http://labstats.citlabs.cornell.edu/LabStats/WebServices/Statistics.asmx?WSDL");

$stats = $client->GetGroupedCurrentStats()->GetGroupedCurrentStatsResult->GroupStat;

print '<ul>';
foreach ($stats as $lab) {
  $labName = $lab->groupName;
  $available = $lab->availableCount;
  $inUse = $lab->inUseCount;
  $percentInUse = round($lab->percentInUse * 100);
  $percentAvailable = 100 - $percentInUse;

  if (stripos($labName, 'mann library') !== false) {
    print '<li><h3>' . $labName . '</h3> ' . $available . ' Available (' . $percentAvailable . '%)</li>';
  }
}
print '</ul>';

// print '<pre>';
// print_r($stats);
// print '</pre>';
?>